import React, { Component } from 'react'

class FormManufacturer extends Component {
  state = {
    name: "",
  }
  handleChange = (event) => {
    const {name, value} = event.target;
    this.setState({[name]: value})
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};

    const mansUrl = "http://localhost:8100/api/manufacturers/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
      }
    };
    const response = await fetch(mansUrl, fetchConfig);
    if (response.ok) {
        const cleared = {
        name: '',
    };
    this.setState(cleared);
  }
}

  render() {
    return (
      <div className="mb-3">
        <h1 className="form-name"> Create a Manufacturer </h1>
        <form className='form' onSubmit={(e) => this.handleSubmit(e)}>
            <input onChange={this.handleChange} required name="name" id="name" className="form-control" value={this.state.name} />
            <button id="submit-button" className="btn btn-primary">Create Manufacturer</button>
        </form>
      </div>
    )
    };
}

export default FormManufacturer
