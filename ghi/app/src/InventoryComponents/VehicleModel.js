import React, {useEffect, useState} from 'react'
import FormModel from './FormModel'

function Inventory() {

  const [models, setModel] = useState([]);

  async function getModels() {
    let listUrl = "http://localhost:8100/api/models/";
    try {
      const response = await fetch(listUrl);
      const listModels = await response.json();
      setModel(listModels.models);
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(
    () => {
        getModels();
    }, []
  )

  return (
    <div>
      <FormModel/>
      <div className="app-container">
          <table>
              <thead>
                  <tr>
                      <th >Manufacturer</th>
                      <th>Model</th>
                      <th>Image</th>
                  </tr>
              </thead>
              <tbody>
                {models.map(model => {
                return(
                  <tr key={model.id}>
                    <td>{model.manufacturer.name}</td>
                    <td>{model.name}</td>
                    <td>{model.picture_url}</td>
                  </tr>
                )
                })}
              </tbody>
          </table>
      </div>
    </div>
  )
}

export default Inventory
