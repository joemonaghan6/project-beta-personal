import React, {useState, useEffect, useRef} from 'react'

function FormAutomobile() {
    const blankForm = {
        color: "",
        year: "",
        vin: "",
        model: "",
    }

    const [models, setModel] = useState([]);
    const [automobile, setAutomobile] = useState(blankForm)

    async function getModels() {
        const listUrl = "http://localhost:8100/api/models/";
        try {
          const response = await fetch(listUrl);
          const listOfModels = await response.json();
          setModel(listOfModels.models);
        } catch (e) {
          console.error(e);
        }
      }

    async function postingData(event) {
        event.preventDefault();
        const incomingData = {...automobile};
        const url = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(incomingData),
            headers: {
                'Content-Type': 'appointment/json',
            }
        }

        const response = await fetch(url, fetchConfig);
        if(response.ok){
            const newAutomobile = await response.json();
            console.log("New automobile:", newAutomobile)
        }
    }

    const myForm = useRef(null);

    const resetState = (e) => {
        postingData(e);
        setAutomobile(blankForm);
        myForm.current.reset();
    }

    useEffect(
    () => {
        getModels();
    }, []
    )


  return (
    <div>
        <div className="form-container">
          <h1 className="form-name">Add an automobile to inventoy</h1>
          <form ref={myForm} onSubmit={e => resetState(e)} className="rowsies">
              <div className="form-group">
                  <label>Color</label>
                  <input defaultValue={automobile.color} onChange={e => setAutomobile({...automobile, color: e.target.value})} type="text" className="form-control" id="tech-form1" placeholder="Color.."/>
              </div>
              <div className="form-group">
                  <label> Year </label>
                  <input defaultValue={automobile.year} onChange={e => setAutomobile({...automobile, year: e.target.value})} type="text" className="form-control" id="tech-form1" placeholder="Year ..."/>
              </div>
              <div className="form-group">
                  <label> VIN </label>
                  <input defaultValue={automobile.vin} onChange={e => setAutomobile({...automobile, vin: e.target.value})} type="text" className="form-control" id="tech-form1" placeholder="vin..."/>
              </div>
              <div className="form-group">
                  <label>Automobile Model</label>
                  <select onChange={e => setAutomobile({...automobile, model: e.target.value})} >
                    {models.map(model => {
                        return(
                            <option key={model.id} value={model.id}> {model.name}</option>
                        )
                    })}
                  </select>
                  </div>
              <button type="submit" id="submit-button" className="btn btn-primary">Submit</button>
              </form>
            </div>
    </div>
  )
}

export default FormAutomobile
