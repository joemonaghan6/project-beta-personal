import React, {useState, useRef}  from 'react'
import '../App.css'

function TechnicianForm() {

    const [technician, setTechnician] = useState({name: '', employee_number: ''})

    async function postingData(event) {
        event.preventDefault();
        const incomingData = {...technician};
        const url = "http://localhost:8080/cars/technicians/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(incomingData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig);
        if(response.ok){
            const newTech = await response.json();
        }
    }

    const myForm = useRef(null);
    const resetState = (e) => {
        postingData(e);
        setTechnician({name: '', employee_number: ''})
        myForm.current.reset();
    }

  return (
    <div>
        <h1 className="form-name">New Technician Form</h1>
        <form ref={myForm} onSubmit={e => resetState(e)}>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Technician Name</label>
                <input defaultValue={technician.name} onChange={e => setTechnician({...technician, name: e.target.value})} type="text" className="form-control" id="tech-form" placeholder="Enter name"/>
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputPassword1">Employee Number</label>
                <input defaultValue={technician.employee_number} onChange={e => setTechnician({...technician, employee_number: e.target.value})} type="number" className="form-control" id="tech-form" placeholder="#"/>
            </div>
            <button type="submit" id="submit-button" className="btn btn-primary" >Submit</button>
            {/* <button type="submit" onClick={(event) => {postingData(event)}} className="btn btn-primary">Submit</button> */}
            </form>
    </div>
  )
}

export default TechnicianForm
