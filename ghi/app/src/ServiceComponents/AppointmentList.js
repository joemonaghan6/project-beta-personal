import React, { useState, useEffect } from 'react';
import '../App.css';

function AppointmentList() {

  const HOUR_OPTIONS = {hour: '2-digit', minute: '2-digit'};
  const [appointments, setAppointment] = useState([]);
  const [searchTerm, setSearchTerm] = useState();
  const [searchResults, setSearchResults] = useState([]);

  async function getAppointments() {
    let appointmentsUrl = "http://localhost:8080/cars/appointments/"
    try{
      const response = await fetch(appointmentsUrl);
      const listAppointments = await response.json();
      setAppointment(listAppointments);
      setSearchResults(listAppointments);
    } catch (e) {
      console.error(e);
    }
  }

  function convertTime(time) {
    let aptDateTime = new Date(time)
    let aptTime = aptDateTime.toLocaleTimeString(undefined, HOUR_OPTIONS)
    return aptTime
  }

  useEffect(
    () => {
      getAppointments();
    }, []
  )

  useEffect(
    () => {
      if(searchTerm){
        let filteredResults = appointments.filter(app => app.car_VIN.includes(searchTerm)).filter(app => (app.completed != true))
        console.log("FILTERED RESULTS:", filteredResults)
        setSearchResults(filteredResults);
        console.log("search results here:", searchResults)
      }
    }, [searchTerm]
  )

  const handleCancel = async (app_id) => {
    const deleteUrl = `http://localhost:8080/cars/appointments/${app_id}`
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json"
      }
    }
    let response = await fetch(deleteUrl, fetchConfig)
    getAppointments();
  }

  const completedTrue = async (app) => {
    const updateUrl = `http://localhost:8080/cars/appointments/${app.id}/`
    const tech_id = app.technician.id
    const appointment = {...app, completed: true, technician: tech_id}
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(appointment),
      headers: {
        "Content-Type": "application/json"
      }
    }
    let response = await fetch(updateUrl, fetchConfig)
    getAppointments();
  }

  return (
    <div className="app-list">
      <h1 className="form-name-s">Upcoming Service Appointments
      <input type="text" className="search-bar" placeholder="Search by vin..." onChange={(event) => {setSearchTerm(event.target.value)}}></input></h1>
      <div className="app-container">
          <table>
              <thead>
                  <tr>
                      <th>Customer Name</th>
                      <th>VIP Status</th>
                      <th>VIN</th>
                      <th id="datesh">Date</th>
                      <th id="datesh">Time</th>
                      <th>Technician</th>
                      <th>Reason</th>
                      <th>Status</th>
                  </tr>
              </thead>
              <tbody>
                  {searchResults.map(app => {
                  if(!app.completed){
                    return(
                    <tr key={app.id}>
                        <td>{app.customer}</td>
                        <td>{JSON.stringify(app.vip)}</td>
                        <td>{app.car_VIN}</td>
                        <td>{app.date.slice(0, 10)}</td>
                        <td>{convertTime(app.date)}</td>
                        <td>{app.technician.name}</td>
                        <td>{app.reason}</td>
                        <td className="fincan-container">
                          <button onClick={() => completedTrue(app)} className="fincan-f">Complete</button>
                          <button onClick={() => handleCancel(app.id)} className="fincan-c">Cancel</button>
                        </td>
                    </tr>
                  )}
                  })}
              </tbody>
          </table>
      </div>
    </div>
  )
}

export default AppointmentList
