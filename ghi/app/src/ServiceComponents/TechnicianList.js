import React, {useState, useEffect} from 'react'
import TechnicianForm from './TechnicianForm'

function TechnicianList() {

  const [technicians, setTechnician] = useState([]);

  async function getTechnicians() {
    let listUrl = "http://localhost:8080/cars/technicians/"
    try {
      const response = await fetch(listUrl);
      const listOfTechnicians = await response.json();
      setTechnician(listOfTechnicians)
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(
    () => {
      getTechnicians();
    }, []
  )

  return (
    <div>
      <TechnicianForm/>
        <div className="app-container">
          <table>
              <thead>
                  <tr>
                      <th>Technician</th>
                      <th>Employee ID</th>
                  </tr>
              </thead>
              <tbody>
                  {technicians.map((tech) => {
                  return(
                  <tr key={tech.id}>
                      <td>{tech.name}</td>
                      <td>{tech.employee_number}</td>
                  </tr>
                  )
                  })}
              </tbody>
          </table>
        </div>
    </div>

  )
}

export default TechnicianList
