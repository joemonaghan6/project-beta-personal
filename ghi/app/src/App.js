import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './ServiceComponents/TechnicianList';
import './App.css';
import ParentAppointment from './ServiceComponents/ParentAppointment';
import History from './ServiceComponents/History';
import Inventory from './InventoryComponents/VehicleModel';
import SalesList from './SalesComp/SalesList'
import SalesPersonForm from './SalesComp/SalesPForm'
import CustomerForm from './SalesComp/CustomerForm'
import SaleForm from './SalesComp/SaleForm'
import ManufacturerList from './InventoryComponents/ManufacturerList';
import FormManufacturer from './InventoryComponents/FormManufacturer';
import FormAutomobile from './InventoryComponents/FormAutomobile';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/Appointments/" element={<ParentAppointment />} />
          <Route path="/Technicians/" element={<TechnicianList/>} />
          <Route path="/History/" element={<History/>} />
          <Route path="/Inventory/" element={<Inventory/>} />
          <Route path="/Sales/Person/Form/" element={<SalesPersonForm />} />
          <Route path="/Customer/Form/" element={<CustomerForm />} />
          <Route path="/Sales/List/" element={<SalesList />} />
          <Route path="/Sale/Form/" element={<SaleForm />} />
          <Route path="/Manufacturer/Form/" element={<FormManufacturer />} />
          <Route path="/Manufacturers/List/" element={<ManufacturerList />} />
          <Route path="/Automobile/Form/" element={<FormAutomobile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
