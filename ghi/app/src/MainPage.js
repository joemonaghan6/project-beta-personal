import './App.css'
import barbie from './images/barbie.jpg'
import lexus from './images/beta-proj-lexus-pic.jpg'
import logo from './images/main_logo.png'

function MainPage() {
  return (
    <div>
      <div>
      <h1 className="carcar-title"> <img src={logo}></img></h1>
      <p className="paragraphs">
          The premiere dealership
          platform
      </p>
      <img src={lexus}></img>
      </div>
    </div>
  );
}

export default MainPage;
