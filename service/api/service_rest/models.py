from django.db import models
from django.utils import timezone


class Technician(models.Model):
    name = models.CharField(max_length=50, null=True)
    employee_number = models.IntegerField(null=True)

    def __str__(self):
        return f'{self.name}: employee id: {self.employee_number}'


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=50, unique=True, null=True)
    car_VIN = models.CharField(max_length=50, unique=True, null=True)

    def __str__(self):
        return f'VIN: {self.car_VIN}'


class Appointment(models.Model):
    car_VIN = models.CharField(max_length=50, null=True)
    customer = models.CharField(max_length=50, null=True)
    date = models.DateTimeField(null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True
    )
    reason = models.CharField(max_length=200, null=True)
    vip = models.BooleanField(default=False, null=True, blank=True)
    completed = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return f'Appointment for: {self.customer} on {self.date} is {self.status}'
