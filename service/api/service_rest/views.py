from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
from common.encoders import VinsListEncoder, AppointmentListEncoder, AppointmentDetailEncoder, TechnicianListEncoder
import json


@require_http_methods(["GET"])
def list_all_car_vins(request):
    try:
        vins = AutomobileVO.objects.all()
        return JsonResponse(
            vins,
            encoder=VinsListEncoder,
            safe=False
        )
    except AutomobileVO.DoesNotExist:
        return JsonResponse(
            {"message": "No Automobile VO's exist yet"},
            status=404
        )


@require_http_methods(["GET", "POST", "OPTIONS"])
def get_or_create_technician(request):
        if request.method == "GET":
            technicians = Technician.objects.all()
            if(len(technicians) == 0):
                return []
            else:
                return JsonResponse(
                    technicians,
                    encoder=TechnicianListEncoder,
                    safe=False
                )
        else:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            if(technician):
                return JsonResponse(
                    technician,
                    encoder=TechnicianListEncoder,
                    safe=False
                )
            else:
                raise ValueError("Key-Value inputs do not match expected input types")


@require_http_methods(["GET", "POST"])
def list_or_create_appointments(request, appointment_vin=None):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            appointments,
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        tech_name = content["technician"]
        technician = Technician.objects.get(name=tech_name)
        content["technician"] = technician
        appointment_vin = content["car_VIN"]
        try:
            vip = AutomobileVO.objects.get(car_VIN=appointment_vin)
            if vip:
                content["vip"] = True
                appointment = Appointment.objects.create(**content)
        except AutomobileVO.DoesNotExist:
            appointment = Appointment.objects.create(**content)

        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "PUT"])
def delete_or_put_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.get(pk=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        tech_id = content["technician"]
        technician = Technician.objects.get(id=tech_id)
        content["technician"] = technician
        Appointment.objects.filter(pk=pk).update(**content)
        app = Appointment.objects.get(pk=pk)
        return JsonResponse(
            app,
            encoder=AppointmentListEncoder,
            safe=False
        )
