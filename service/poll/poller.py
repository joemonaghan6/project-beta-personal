import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
from service_rest.models import AutomobileVO
# from service_rest.models import Something

def get_VIN():
    url = "http://inventory-api:8000/api/automobiles/"
    print('Service poller polling for data')
    response = requests.get(url)
    content = json.loads(response.content)
    for car in content["autos"]:
        AutomobileVO.objects.update_or_create(
            import_href=car["href"],
            defaults={
                "car_VIN": car["vin"]
            }
        )

def poll():
    while True:
        print("polling for data")
        try:
            get_VIN()
            print("i gots the data")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
