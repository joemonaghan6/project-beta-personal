from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
#from .acls import get_photo,
from .models import SalesPerson, Customer, Sales, AutomobileVO

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["VIN", "import_href"]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number"
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class SalesListEncoder(ModelEncoder):
    model = Sales
    properties = [
        "price",
        "customer", 
        "salesPerson",
        "VIN",
        
    ]
    encoders = {
    "customer": CustomerListEncoder(),
     "salesPerson": SalesPersonListEncoder(),
     "VIN": AutomobileVOEncoder(),
     }


        

@require_http_methods(["GET", "POST"])
def api_list_sales(request):

    if request.method == "GET":
        sales = Sales.objects.all()

        return JsonResponse( {
            "sales": sales },
            encoder=SalesListEncoder,
            safe = False,
        )

    else: # This is now our post request
        content = json.loads(request.body)
        print(content)

        #These three contents replace all of the bin code from previous projects. 
        #Assigining the post requests content key to foreign key
        content["VIN"] = AutomobileVO.objects.get(VIN = content["VIN"])
    
        content["salesPerson"] = SalesPerson.objects.get(id = content["salesPerson"])

        content["customer"] = Customer.objects.get(id = content["customer"])

        sales = Sales.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SalesListEncoder,
            safe=False,
        )
# End GET POST Function for list sales

# Get our unsold cars using exclude and not adding sold property to the model
@require_http_methods(["GET"])
def api_unsold_cars(request):
    sold_cars = [sales.VIN.VIN for sales in Sales.objects.all()]
    # unsold_cars =  [auto.VIN for auto in AutomobileVO.objects.all() if auto.VIN not in sold_cars] 
    unsold_cars = AutomobileVO.objects.exclude(VIN__in=sold_cars)
    

    return JsonResponse( {
            "Automobiles": unsold_cars },
            encoder=AutomobileVOEncoder,
            safe = False,
    )


@require_http_methods(["GET", "POST"])
def api_customer_form(request):
    
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse( {
            "customer": customers },
            encoder=CustomerListEncoder,
            safe = False,
        )
    else: # This is now our post request
        content = json.loads(request.body)
        

    customers = Customer.objects.create(**content)
    return JsonResponse(
        customers,
        encoder=CustomerListEncoder,
        safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_employee_form(request):
    
    if request.method == "GET":
        employees = SalesPerson.objects.all()
        return JsonResponse( {
            "salesPerson": employees },
            encoder=SalesPersonListEncoder,
            safe = False,
        )
    else: # This is now our post request
        content = json.loads(request.body)
        

    employees = SalesPerson.objects.create(**content)
    return JsonResponse(
        employees,
        encoder=SalesPersonListEncoder,
        safe=False,
        )
